FROM node:10
LABEL maintainer="christophe.benz@jailbreak.paris"

EXPOSE 3000

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build

CMD node __sapper__/build
