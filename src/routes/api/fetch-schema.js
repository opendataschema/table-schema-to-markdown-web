import fetch from "node-fetch"

export async function get(req, res) {
  const { url } = req.query
  console.log(`fetching schema ${url}`)

  let response
  try {
    response = await fetch(url)
  } catch (err) {
    console.error(err)
    return res.status(500).end(err.message || "Internal Server Error")
  }
  if (!response.ok) {
    return res.status(response.status).end(response.statusText || "Internal Server Error")
  }

  let schemaJson
  try {
    schemaJson = await response.json()
  } catch (err) {
    console.error(err)
    return res.status(500).end(err.message || "Invalid JSON")
  }

  return res.json(schemaJson)
}
