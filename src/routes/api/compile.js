import { toMarkdown } from "@opendataschema/table-schema-to-markdown"

export async function post(req, res) {
  const { schema, locale = "en" } = req.body

  let markdown
  try {
    markdown = await toMarkdown(schema, { locale })
  } catch (err) {
    console.error(err)
    return res.status(500).end("Internal Server Error")
  }

  res.setHeader("Content-Type", "text/markdown")
  return res.end(markdown)
}
