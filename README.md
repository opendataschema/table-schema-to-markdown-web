# Table Schema to Markdown – web UI

Try it here: https://table-schema-to-markdown.herokuapp.com/

## Development

```bash
git clone https://framagit.org/opendataschema/table-schema-to-markdown-web.git
cd table-schema-to-markdown-web
npm install
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.
